public class Task08 {
    public static void main(String[] args) {
        int count = 0;
        for (int i = 0; i < 24; i++){
            int firstDigit = i%10;
            int secondDigit = i/10;
            if ((firstDigit * 10 + secondDigit) < 60) {
                System.out.printf("%d%d:%d%d%n", secondDigit, firstDigit, firstDigit, secondDigit);
                count++;
            }
        }
        System.out.print(count);
    }
}
