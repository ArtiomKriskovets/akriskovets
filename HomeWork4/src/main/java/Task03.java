public class Task03 {
    public static void main(String[] args) {
        for (int i = 10; i <= 1000000; i++)
            if (isArmstrongNumber(i)) System.out.printf("%d ", i);
    }


    public static boolean isArmstrongNumber (int number){
        int summ = 0;
        int temp = number;

        int power = powerArmstrongNumber(number);

        while(number!=0){
            summ += Math.pow(number%10, power) ;
            number = number/10;
        }

        return (summ == temp);
    }


    public static int powerArmstrongNumber (int number){
        int result = 0;

        while (number != 0){
            number /= 10;
            result++;
        }

        return result;
    }
}
