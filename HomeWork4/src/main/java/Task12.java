import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        float x = 5;
        Scanner sc = new Scanner(System.in);
        float y = sc.nextFloat();
        int days = 1;

        while (x < y){
            x *= 1.1;
            days++;
        }

        System.out.print(days);
    }
}
