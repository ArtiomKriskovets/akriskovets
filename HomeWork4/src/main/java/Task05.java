import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        StringBuilder strBuilder = new StringBuilder(Integer.toString(number));

        if (strBuilder.toString().equals(strBuilder.reverse().toString())) System.out.print("Yes");
        else System.out.print("No");
    }
}
