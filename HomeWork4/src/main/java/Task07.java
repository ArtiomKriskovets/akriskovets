import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        printBinaryNumber(number);
    }

    private static void printBinaryNumber(int number){
        if (number != 0){
            printBinaryNumber(number / 2);
            System.out.print(number % 2);
        }
    }
}
