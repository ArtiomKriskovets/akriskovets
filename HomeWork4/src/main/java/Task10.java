public class Task10 {
    public static void main(String[] args) {
        int number = 2520;
        boolean resultFlag = false;
        while (resultFlag == false){
            number++;
            resultFlag = operatingWithDividers(number);
        }
        System.out.print(number);
    }

    private static boolean operatingWithDividers(int number){
        boolean flag = true;

        for (int i = 1; i <= 20; i++)
            if (number%i != 0){
                flag = false;
                break;
            }

        return flag;
    }
}
