public class Task02 {
    public static void main(String[] args) {
        for (int i = 3; i < 1000000; i++){
            int flag = 0;
            for (int j = 2; j < (Math.sqrt(i)+1); j++){
                if (i%j == 0) flag++;
                if (flag == 1) break;
            }
            if (flag == 0) System.out.printf("%d ", i);
        }
    }
}
