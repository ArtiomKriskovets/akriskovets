import java.util.Arrays;

public class Task06 {
    public static void main(String[] args) {
        for (int i = 10000000; i <= 99999999; i++)
        if ((isUniqueDigits(i))&&((i%12345) == 0)) System.out.printf("%d ", i);
    }

    private static boolean isUniqueDigits (int number){
        char[] numArr =  Integer.toString(number).toCharArray();
        Arrays.sort(numArr);
        if ((numArr[0]<numArr[1])&&(numArr[1]<numArr[2])&&(numArr[2]<numArr[3])&&(numArr[3]<numArr[4])&&(numArr[4]<numArr[5])&&(numArr[5]<numArr[6])&&(numArr[6]<numArr[7])) return true;
        else return false;

    }
}
