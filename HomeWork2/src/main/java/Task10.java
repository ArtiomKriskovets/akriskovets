

import java.io.IOException;

/**
 * Определить номер подъезда девятиэтажного дома по указанному номеру квартиры
 * N. Считать, что на каждом этаже находится M квартир. Вывести в консоль номер
 * подъезда.
 * 
 */
public class Task10
{
    public static void main(String[] args) throws IOException
    {
        int room = 136;
        int roomsOnFloor = 5;

        if (room%(9 * roomsOnFloor) != 0) System.out.print((room/(9 * roomsOnFloor)) + 1);
        else System.out.print(room/(9 * roomsOnFloor));
    }
}