

import java.util.Scanner;

/**
 * Определить количество дней в году, который вводит пользователь и вывести их в
 * консоль. В високосном годе - 366 дней, тогда как в обычном 365. Високосными
 * годами являются все года делящиеся без остатка на 4 за исключением столетий,
 * которые не делятся нацело на 400
 * 
 */
public class Task06
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();

        if (year%100 == 0){
            if (year%400 == 0) System.out.print("366");
            else System.out.print("365");
        }
        else if ( year%4 == 0 ) System.out.print("366");
        else System.out.print("365");
    }
}