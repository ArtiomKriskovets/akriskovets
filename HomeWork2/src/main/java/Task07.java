

import java.io.IOException;
import java.util.Scanner;

/**
 * Пользовталь вводит с клавиатуры букву, программа должна определить, в какой
 * раскладке введена буква, в латинской или кирилице. Вывести в консоль:
 * «латиница» если буква введена латиницей, и «кирилица» если буква относится к
 * кирилическому алфавиту. Если введена цифра а не буква, вывести «цифра». Если
 * символ не относится ни к буквам ни к цифрам вывести «невозможно определить».
 * Программа не долджна зависеть от регистра букв. Подсказка: Символы в таблице
 * UNICODE распологаюся в алфавитном порядке.
 * 
 * 
 */
public class Task07
{
    public static void main(String[] args) throws IOException
    {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.next().charAt(0);

        if (Character.isLetterOrDigit(letter)) {
            if (((letter >= 'A') && (letter <= 'Z')) || ((letter >= 'a') && (letter <= 'z'))) {
                System.out.print("Latin.");
            }
            else if ((letter >= 'А') && (letter <= 'я')) {
                System.out.print("Cyrillic.");
            }
            else if (Character.isDigit(letter)) System.out.print("Numeral.");
            else System.out.print("Unable to determine.");
        }
        else System.out.print("Unable to determine.");
    }
}