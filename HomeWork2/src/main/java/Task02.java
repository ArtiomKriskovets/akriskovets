

import java.util.Scanner;

/**
 * С клавиатуры пользователь вводит время (количество часов от 0 до 24) –
 * программа выводит приветствие, соответствующее введённому времени (например,
 * ввели 15 часов – выводится приветствие «добрый день»).
 */
public class Task02
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int time = scanner.nextInt();

        if ( (time >= 0) && (time <= 24) ) {
            if (((time >= 6) && (time < 12)) || ((time >= 0) && (time < 6 ))) System.out.print("Good morning!");
            if ((time >= 12) && (time < 18)) System.out.print("Good afternoon!");
            if ((time >= 18) && (time <= 24)) System.out.print("Good evening!");
            }
        else System.out.print("Invalid input.");
}
}