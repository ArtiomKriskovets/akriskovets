

import java.util.Scanner;

/**
 * Организовать ввод с клавиатуры даты рождения человека, программа должна
 * вывести знак зодиака и названия года по китайскому календарю. 
 * Пример входных данных: 5 12 1974 
 * Вывод: Знак: стрелец Год: тигра
 * 
 */
public class Task05
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        int birthDay = scanner.nextInt();
        int birthMonth = scanner.nextInt();
        int birthYear = scanner.nextInt();

        System.out.print("Sign of the zodiac: ");

        if (((birthMonth == 1) && (birthDay >= 21)) || ((birthMonth == 2) && (birthDay <= 20))) System.out.print("Aquarius. ");
        if (((birthMonth == 2) && (birthDay >= 21)) || ((birthMonth == 3) && (birthDay <= 20))) System.out.print("Pisces. ");
        if (((birthMonth == 3) && (birthDay >= 21)) || ((birthMonth == 4) && (birthDay <= 20))) System.out.print("Aries. ");
        if (((birthMonth == 4) && (birthDay >= 21)) || ((birthMonth == 5) && (birthDay <= 20))) System.out.print("Taurus. ");
        if (((birthMonth == 5) && (birthDay >= 21)) || ((birthMonth == 6) && (birthDay <= 21))) System.out.print("Gemini. ");
        if (((birthMonth == 6) && (birthDay >= 22)) || ((birthMonth == 7) && (birthDay <= 22))) System.out.print("Cancer. ");
        if (((birthMonth == 7) && (birthDay >= 23)) || ((birthMonth == 8) && (birthDay <= 23))) System.out.print("Leo. ");
        if (((birthMonth == 8) && (birthDay >= 24)) || ((birthMonth == 9) && (birthDay <= 23))) System.out.print("Virgo. ");
        if (((birthMonth == 9) && (birthDay >= 24)) || ((birthMonth == 10) && (birthDay <= 23))) System.out.print("Libra. ");
        if (((birthMonth == 10) && (birthDay >= 24)) || ((birthMonth == 11) && (birthDay <= 22))) System.out.print("Scorpio. ");
        if (((birthMonth == 11) && (birthDay >= 23)) || ((birthMonth == 12) && (birthDay <= 21))) System.out.print("Sagittarius. ");
        if (((birthMonth == 12) && (birthDay >= 22)) || ((birthMonth == 1) && (birthDay <= 20))) System.out.print("Capricorn. ");

        System.out.print("Year of the ");

        switch (birthYear%12) {
            case 0: System.out.print("Monkey.");
                break;
            case 1: System.out.print("Rooster.");
                break;
            case 2: System.out.print("Dog.");
                break;
            case 3: System.out.print("Pig.");
                break;
            case 4: System.out.print("Rat.");
                break;
            case 5: System.out.print("Ox.");
                break;
            case 6: System.out.print("Tiger.");
                break;
            case 7: System.out.print("Rabbit.");
                break;
            case 8: System.out.print("Dragon.");
                break;
            case 9: System.out.print("Snake.");
                break;
            case 10: System.out.print("Horse.");
                break;
            case 11: System.out.print("Ram.");
                break;
        }

    }
}