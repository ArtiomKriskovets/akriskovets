

import java.io.IOException;

/**
 * Даны координаты начала и координаты конца отрезка. Если считать
 * отрезок обозначением горки, то в одном случае он обозначает спуск, в другой
 * подъем. Определить и вывести на экран спуск это или подъем, ровная дорога или
 * вообще отвестная.
 * Пример Ввод 1 1 2 2 Вывод подъем
 * Пример Ввод 1 1 2 1 Вывод ровно
 * 
 */
public class Task09
{
    public static void main(String[] args) throws IOException
    {
        int x1 = 6;
        int y1 = 5;
        int x2 = 1;
        int y2 = 1;

        if ((x1==x2)&(y1==y2)) System.out.print("Incorrect input.");
        else {
            if (x1==x2) System.out.print("Vertical.");
            else if (y1==y2) System.out.print("Smooth.");
            else if (y1>y2) System.out.print("Downhill.");
            else System.out.print("Climb.");

        }
    }
}