

import java.io.IOException;

/**
 * Найти корни квадратного уравнения и вывести их на экран, если они есть. Если
 * корней нет, то вывести сообщение об этом. Конкретное квадратное уравнение
 * определяется коэффициентами a, b, c, которые вводит пользователь с
 * клавиатуры.
 * 
 */
public class Task11
{
    public static void main(String[] args) throws IOException
    {
        int a = 3;
        int b = 1;
        int c = 3;
        int discriminant = (b * b) - (4 * a * c);

        System.out.printf("%dx^2+%dbx + %d=0\n", a, b, c);

        if (discriminant < 0) System.out.print("The equation has no roots.");

        if (discriminant == 0) {
            float x = (float)(-b) / (2*a);
            System.out.print("The equation has one root. x = " + x);
        }

        if (discriminant > 0) {
            float x1 = (float)( - b + Math.sqrt(discriminant))/(2 * a);
            float x2 = (float)( - b - Math.sqrt(discriminant))/(2 * a);
            System.out.print("The equation has two roots. x1 = " + x1 + "; x2 = " + x2 + ";");
        }
    }
}