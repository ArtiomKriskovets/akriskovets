

import java.util.Scanner;

/**
 * Дана точка на плоскости заданная координатами x и y, определить и вывести в
 * консоль, в какой четверти находится точка. в прямоугольной системе
 * (декартовой) координат. Четверти обозначены римскими цифрами.
 * Прием x=1 y=11 Вывод I
 * Прием x=-2 y=-3 Вывод III
 */
public class Task04
{
    public static void main(String[] args)
    {
        int x = 1;
        int y = 0;

        if (x == 0 & y == 0) System.out.print("Point belongs to origin.");
        else if ((x == 0) || (y == 0)) {
            if (x == 0) System.out.print("Point belongs to x axis.");
            if (y == 0) System.out.print("Point belongs to y axis.");
        }
        else {
            if ((x > 0) && (y > 0)) System.out.print("I");
            if ((x < 0) && (y > 0)) System.out.print("II");
            if ((x < 0) && (y < 0)) System.out.print("III");
            if ((x > 0) && (y < 0)) System.out.print("IV");
        }
        
    }
}