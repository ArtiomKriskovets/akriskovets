

import java.io.IOException;
import java.util.Scanner;

/**
 * Программа запрашивает шестизначное число, после ввода определяет будет ли
 * являтся счастливым билет с таким номером (сумма первых трех цифр совпадает с
 * сумой трех последних). Если число не шестизначное, вывести сообщение об ошибке. 
 * Пример входных данных: 423151 954832 Вывод: Да Нет
 * 
 * 
 */
public class Task12
{
    public static void main(String[] args) throws IOException
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a six-digit number:");
        int number = scanner.nextInt();

        if ( (number < 100000) || (number > 999999)){
            System.out.print("Incorrect input.");
        }
        else {
            int firstHalf = number / 1000;
            int secondHalf = number % 1000;

            if (summ(firstHalf) == summ(secondHalf)) System.out.print("Yes.");
            else System.out.print("No.");
        }
    }
    
    private static int summ(int half) {
        int result = (half / 100) + (half % 10) + (half / 10) % 10;
        return result;
    }
}