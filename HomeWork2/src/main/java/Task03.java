

import java.util.Scanner;

/**
 * Есть три переменных a, b и с эти переменный явно записаны три целых попарно
 * неравных между собой числа. Создать программу, которая переставит числа в
 * переменных таким образом, чтобы при выводе на экран последовательность a, b и
 * c оказалась строго возрастающей. Пример 7 4 5 Вывод 457
 * 
 */
public class Task03
{
    public static void main(String[] args)
    {
        int a = 4;
        int b = 5;
        int c = 7;

        int aTemp = a;
        int bTemp = b;
        int cTemp = c;

        if ((b < a) && (b < c)) {
            aTemp = b;
            if (a < c) {
                bTemp = a;
            }
            else {
                bTemp = c;
                cTemp = a;
            }
        }
        if ((c < b) && (c < a)) {
            aTemp = c;
            if (b < a) {
                cTemp = a;
            }
            else {
                bTemp = a;
                cTemp = b;
            }
        }
        if ((a < b) && (a < c)) {
            if (c < b) {
                bTemp = c;
                cTemp = b;
            }
        }

        a = aTemp;
        b = bTemp;
        c = cTemp;

        // TODO не менять строку вывода
        System.out.printf("%d %d %d", a, b, c);
    }
}