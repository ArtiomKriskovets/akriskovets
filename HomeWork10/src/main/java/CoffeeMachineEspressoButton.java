public interface CoffeeMachineEspressoButton {
    public void buttonOn();
    public void buttonOff();
    public void buttonMakeEspresso();
    public void buttonMakeAmericano();
}
