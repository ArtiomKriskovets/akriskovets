public interface CoffeeMachineError {
    public void printErrorNoWater();
    public void printErrorNoCoffee();
    public void printErrorWastedCoffeeContainerIsFull();
}
