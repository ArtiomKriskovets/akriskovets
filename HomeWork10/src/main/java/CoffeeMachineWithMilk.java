public class CoffeeMachineWithMilk extends CoffeeMachine implements CoffeeMachineWithMilkButton {

    private int milkValue;
    private int maxMilkValue;

    public CoffeeMachineWithMilk(String name, int maxCoffeeValue, int maxWaterValue, int maxWastedCoffeValue, int maxMilkValue) {
        super(name, maxCoffeeValue, maxWaterValue, maxWastedCoffeValue);
        this.maxMilkValue = maxMilkValue;
    }


    public void buttonMakeCapuchino() {
        if (conditionCheck(22, 50)) {
            if (checkMilk(20)) {
                makeCoffee(22,50);
                addMilkInCup(20);
            }
        }
    }

    public void buttonMakeLatte() {
        if (conditionCheck(22, 50)) {
            if (checkMilk(70)) {
                makeCoffee(22,50);
                addMilkInCup(70);
            }
        }
    }

    public boolean checkMilk(int milkNeeded) {
        if (milkValue < milkNeeded) {
            printErrorNoMilk();
            return false;
        } else {
            return true;
        }
    }

    public void printErrorNoMilk() {
        printMessage("No milk!");
    }

    public void addMilkInCup(int milkNeeded) {
        milkValue -= milkNeeded;
    }

    public void addMilk(int milk) {
        milkValue += milk;
        if (milkValue > maxMilkValue) {
            milkValue = maxMilkValue;
        }
    }

    public int getMilkValue() {
        return milkValue;
    }



}
