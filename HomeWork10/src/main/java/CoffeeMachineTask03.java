public class CoffeeMachineTask03 extends CoffeeMachineWithMilk {

    int coffeeBeansValue;
    int maxCoffeeBeansValue;

    public CoffeeMachineTask03(String name, int maxCoffeeValue, int maxWaterValue, int maxWastedCoffeValue, int maxMilkValue) {
        super(name, maxCoffeeValue, maxWaterValue, maxWastedCoffeValue, maxMilkValue);
    }

    public void addCoffeBeans(int coffeeBeans) {
        coffeeBeansValue +=coffeeBeans;
        if (coffeeBeansValue > maxCoffeeBeansValue) {
            coffeeBeans = maxCoffeeBeansValue;

        }
    }

    public void buttonMakeEspresso() {
        printMessage("Sorry, i don't make espresso!");
    }

    public void buttonMakeCapuchino() {
        printMessage("Sorry, i don't make capuchino!");
    }

    public boolean checkCoffeeBeans(int coffeBeansNeeded) {
        if (coffeeBeansValue < coffeBeansNeeded) {
            printMessage("Error, no coffee beans!");
            return false;
        }
        return true;
    }

    public void coffeeGrind(int value) {
        if (checkCoffeeBeans(value)) {
            if ((getCoffeeValue() + value) > (getMaxCoffeeValue())) {
                coffeeBeansValue -= getMaxCoffeeValue() - getCoffeeValue();
                addCoffee(getMaxCoffeeValue() - getCoffeeValue());
            } else {
                coffeeBeansValue -= value;
                addCoffee(value);
            }
        }
    }


    public boolean conditionCheck(int coffeeNeeded, int waterNeeded) {
        if (!checkIsOn()) {
            return false;
        }
        if (getCoffeeValue() < coffeeNeeded) {
            coffeeGrind(coffeeNeeded);
            printErrorNoCoffee();
            return false;
        }
        if (getWaterValue() < waterNeeded) {
            printErrorNoWater();
            return false;
        }
        if ((coffeeNeeded + getWastedCoffeeValue()) > getMaxWastedCoffeeValue()) {
            printErrorWastedCoffeeContainerIsFull();
            return false;
        }

        return true;
    }
}
