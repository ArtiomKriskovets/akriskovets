public interface CoffeeMachineOperation {
    public void clearWastedCoffeeContainer();
    public boolean conditionCheck(int coffeeNeeded, int waterNeeded);
}
