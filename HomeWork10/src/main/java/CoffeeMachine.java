public class CoffeeMachine implements CoffeeMachineEspressoButton, CoffeeMachineError, CoffeeMachineOperation{
    private String name;

    private int coffeeValue;
    private int maxCoffeeValue;

    private int waterValue;
    private int maxWaterValue;

    private int wastedCoffeeValue;
    private int maxWastedCoffeeValue;

    private boolean isOn;

    public CoffeeMachine(String name, int maxCoffeeValue, int maxWaterValue, int maxWastedCoffeeValue) {
        this.name = name;
        this.coffeeValue = 0;
        this.maxCoffeeValue = maxCoffeeValue;
        this.waterValue = 0;
        this.maxWaterValue = maxWaterValue;
        this.wastedCoffeeValue = 0;
        this.maxWastedCoffeeValue = maxWastedCoffeeValue;
        isOn = false;
    }

    public String getCoffeeMachineName() {
        return name;
    }

    public void addCoffee(int coffee) {
        coffeeValue += coffee;
        if (coffeeValue > maxCoffeeValue) {
            coffeeValue = maxCoffeeValue;
        }
    }

    public void addWater(int water) {
        waterValue += water;
        if (waterValue > maxWaterValue) {
            waterValue = maxWaterValue;
        }
    }


    public void buttonOn() {
        isOn = true;
    }

    public void buttonOff() {
        isOn = false;
    }

    public void buttonMakeEspresso() {
        if (conditionCheck(22, 30)) {
            makeCoffee(22,30);
        }
    }

    public void buttonMakeAmericano() {
        if (conditionCheck(22, 100)) {
            makeCoffee(22,100);
        }
    }

    public void printErrorNoWater() {
        printMessage("No water!");
    }

    public void printErrorNoCoffee() {
        printMessage("No coffee!");

    }

    public void printErrorWastedCoffeeContainerIsFull() {
        printMessage("Container for easted coffee is full!");
    }

    public void clearWastedCoffeeContainer() {
        wastedCoffeeValue = 0;
    }

    public boolean conditionCheck(int coffeeNeeded, int waterNeeded) {
        if (!isOn) {
            return false;
        }
        if (coffeeValue < coffeeNeeded) {
            printErrorNoCoffee();
            return false;
        }
        if (waterValue < waterNeeded) {
            printErrorNoWater();
            return false;
        }
        if ((coffeeNeeded + wastedCoffeeValue) > maxWastedCoffeeValue) {
            printErrorWastedCoffeeContainerIsFull();
            return false;
        }

        return true;
    }

    public void printMessage(String message) {
        System.out.println(name + ": " + message);
    }

    public int getCoffeeValue() {
        return coffeeValue;
    }

    public int getWaterValue() {
        return waterValue;
    }

    public int getWastedCoffeeValue() {
        return wastedCoffeeValue;
    }

    public boolean checkIsOn() {
        return isOn;
    }

    protected void makeCoffee(int coffee, int water) {
        coffeeValue -= coffee;
        waterValue -= water;
        wastedCoffeeValue += coffee;
    }

    public int getMaxCoffeeValue() {
        return maxCoffeeValue;
    }

    public int getMaxWastedCoffeeValue() {
        return maxWastedCoffeeValue;
    }



}

