public interface CoffeeMachineWithMilkButton  {
    public void buttonMakeCapuchino();
    public void buttonMakeLatte();
}
