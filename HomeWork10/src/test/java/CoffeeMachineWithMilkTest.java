import org.junit.Assert;
import org.junit.Test;

public class CoffeeMachineWithMilkTest {

    CoffeeMachineWithMilk coffeeMachineWithMilk;

    {
        coffeeMachineWithMilk = new CoffeeMachineWithMilk("Model_1", 100, 300, 50, 200);
    }

    @Test
    public void checkMilkTest() {
        coffeeMachineWithMilk.buttonOn();
        Assert.assertEquals(false, coffeeMachineWithMilk.checkMilk(1));
        coffeeMachineWithMilk.addMilk(10);
        Assert.assertEquals(true, coffeeMachineWithMilk.checkMilk(1));
    }

    @Test
    public void buttonMakeLatteTest() {
        coffeeMachineWithMilk.buttonOn();
        coffeeMachineWithMilk.addMilk(100);
        coffeeMachineWithMilk.addCoffee(50);
        coffeeMachineWithMilk.addWater(100);
        coffeeMachineWithMilk.buttonMakeLatte();
        Assert.assertEquals(28, coffeeMachineWithMilk.getCoffeeValue());
        Assert.assertEquals(50, coffeeMachineWithMilk.getWaterValue());
        Assert.assertEquals(30, coffeeMachineWithMilk.getMilkValue());
    }

    @Test
    public void buttonMakeCapuchinoTest() {
        coffeeMachineWithMilk.buttonOn();
        coffeeMachineWithMilk.addMilk(100);
        coffeeMachineWithMilk.addCoffee(50);
        coffeeMachineWithMilk.addWater(100);
        coffeeMachineWithMilk.buttonMakeCapuchino();
        Assert.assertEquals(28, coffeeMachineWithMilk.getCoffeeValue());
        Assert.assertEquals(50, coffeeMachineWithMilk.getWaterValue());
        Assert.assertEquals(80, coffeeMachineWithMilk.getMilkValue());
    }
}
