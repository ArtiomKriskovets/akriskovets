import org.junit.Assert;
import org.junit.Test;

public class CoffeMachineTest {

    CoffeeMachine coffeeMachine;

    {
        coffeeMachine = new CoffeeMachine("Model_1", 100, 300, 50);
    }

    @Test
    public void getCoffeMachineNameTest() {
        Assert.assertEquals("Model_1", coffeeMachine.getCoffeeMachineName());
    }

    @Test
    public void checkIsOnTest() {
        Assert.assertEquals(false, coffeeMachine.checkIsOn());
        coffeeMachine.buttonOn();
        Assert.assertEquals(true, coffeeMachine.checkIsOn());
    }

    @Test
    public void buttonMakeEspressoTest() {
        coffeeMachine.buttonOn();
        coffeeMachine.addCoffee(50);
        coffeeMachine.addWater(200);
        coffeeMachine.buttonMakeEspresso();
        Assert.assertEquals(28, coffeeMachine.getCoffeeValue());
        Assert.assertEquals(170, coffeeMachine.getWaterValue());
    }

    @Test
    public void buttonMakeAmericanoTest() {
        coffeeMachine.buttonOn();
        coffeeMachine.addCoffee(50);
        coffeeMachine.addWater(200);
        coffeeMachine.buttonMakeAmericano();
        Assert.assertEquals(28, coffeeMachine.getCoffeeValue());
        Assert.assertEquals(100, coffeeMachine.getWaterValue());
    }
}
