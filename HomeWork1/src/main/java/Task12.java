/*
 * Шаблон для решения домашнеего задания. 
 */

/**
 * Ученикам первого класса дают 1 пирожок, если вес первоклассника менее 30 кг
 * дополнительно дают 1 стакан молока и ещё 1 пирожок. В первых классах школы
 * учится n учеников. Стакан молока имеет ёмкость 200 мл, а упаковка молока –
 * 0,9 л. Написать программу которая определит количество пакетов молока и
 * пирожков, необходимых каждый день для условий: a) Если в школе 100% всех
 * учеников, у которых вес меньше 30 кг. b) Если в школе 60% учеников, имеют вес
 * меньше 30 кг. c) Если в школе 1% учеников, имеют вес меньше 30 кг.
 * 
 */
public class Task12
{
    public static void main(String[] args)
    {
        int n = 100;

        int n_a = n;
        int n_b = n/10*6;
        int n_c = n/100;

        float milk_a_all = (float)(n_a * 0.2);
        int milk_a_carton = (int)(milk_a_all/0.9);
        if ( (milk_a_carton * 0.9) != milk_a_all ) milk_a_carton++;

        float milk_b_all = (float)(n_b * 0.2);
        int milk_b_carton = (int)(milk_b_all/0.9);
        if ( (milk_b_carton * 0.9) != milk_b_all ) milk_b_carton++;

        float milk_c_all = (float)(n_c * 0.2);
        int milk_c_carton = (int)(milk_c_all/0.9);
        if ( (milk_c_carton * 0.9) != milk_c_all ) milk_c_carton++;

        System.out.println("Для случая А");
        System.out.println(milk_a_carton + " " + (n+n_a) );

        System.out.println("Для случая B");
        System.out.println(milk_b_carton + " " + (n+n_b) );

        System.out.println("Для случая C");
        System.out.println(milk_c_carton + " " + (n+n_c) );
    }
}
