/*
 * Шаблон для решения домашнеего задания. 
 */

/**
 * В переменных х и y хранятся два натуральных числа. Создайте программу,
 * выводящую на консоль: 
 * • Результат целочисленного деления x на y • Остаток от деления x на y. 
 * • Квадратный корень x
 * 
 */
public class Task01
{
    public static void main(String[] args)
    {
        int x = 5;
        int y = 3;
        System.out.println("x/y=" + x/y);
        System.out.println("x%y=" + x%y);
        System.out.println("Sqrt(x)=" + Math.sqrt(x));
    }
}