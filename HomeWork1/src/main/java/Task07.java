/*
* Шаблон для решения домашнеего задания
*/
/**
 * Разработать программу, которая позволит при известном годовом проценте 
 * вычислить сумму вклада в банке через два года,
 * если задана исходная величина вклада. Вывести результат вычисления в консоль.
 *
 */
public class Task07
{
    public static void main(String[] args)
    {
        int depositSumma = 1500;
        int annualPercentage = 3;
        float sum = depositSumma * (1+((float)(annualPercentage * 2)/100));
        System.out.print(sum);
    }
}
