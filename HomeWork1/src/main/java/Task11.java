/*
 * Шаблон для решения домашнеего задания. 
 */
/**
 * Шаблон для решения домашнеего задания
 * У Деда Мороза есть часы, которые в секундах показывают сколько осталось до
 * каждого Нового Года. Так как Дед Мороз уже человек в возрасте, то некоторые
 * математические операции он быстро выполнять не в состоянии. Помогите Деду
 * Морозу определить сколько полных дней, часов, минут и секунд осталось до
 * следующего Нового Года, если известно сколько осталось секунд, т. е.
 * Разложите время в секундах на полное количество дней, часов, минут и секунд.
 * Выведите результат на консоль.
 */
public class Task11
{
    public static void main(String[] args)
    {
        int secondsToNewEar = 127661;
        int daysToNewEar = secondsToNewEar / (60*60*24);
        int hoursToNewEar = ( secondsToNewEar - daysToNewEar * (60*60*24) ) / (60*60);
        int minutesToNewEar = ( secondsToNewEar - (daysToNewEar * (60*60*24) + hoursToNewEar *(60*60)))/60;
        secondsToNewEar = secondsToNewEar - (daysToNewEar * (60 * 60 * 24)) - (hoursToNewEar * (60 * 60)) - (minutesToNewEar * 60);
        System.out.print(daysToNewEar + " " + hoursToNewEar + " " + minutesToNewEar + " " + secondsToNewEar);
        // данная строка нужна для автоматического теста (смотри Task11Test) 
        secondsToNewEar = (args.length == 1) ? Integer.valueOf(args[0]) : secondsToNewEar;
        // используй переменную SECONDS_TO_NEW_YEAR для решения
    }
}