public class Task08 {
    public static void main(String[] args) {
        int n = 238;
        int bitNumber = (int)(Math.log(n)/Math.log(2));
        int result =  n & ~(1 << bitNumber ) ;

        System.out.println(Integer.toBinaryString(n));
        System.out.println(Integer.toBinaryString(result));
    }
}
