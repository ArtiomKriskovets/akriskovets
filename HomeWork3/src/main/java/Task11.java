public class Task11 {
    public static void main(String[] args) {
        int n = 157;
        System.out.println(Integer.toBinaryString(n));
        int bitNumber = 0;
        while (n != 0) {
            bitNumber += n & 1;
            n = n >>> 1;
        }
        System.out.print(bitNumber);
    }
}
