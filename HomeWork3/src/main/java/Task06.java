public class Task06 {
    public static void main(String[] args) {
        int n = 157;
        int i = 4;
        int bitNumber = (int)(Math.log(n)/Math.log(2)) + 1;

        System.out.println(Integer.toBinaryString(n));

        n = n >> (bitNumber - i);
        n = n << (bitNumber - i);

        System.out.println(Integer.toBinaryString(n));
    }
}
