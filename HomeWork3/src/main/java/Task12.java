public class Task12 {
    public static void main(String[] args) {
        int n = 157;

        System.out.println(Integer.toBinaryString(n));

        int bitNumber = 0;
        int allNumber = 0;

        while (n != 0) {
            bitNumber += n & 1;
            n = n >>> 1;
            allNumber++;
        }
        if ((allNumber - bitNumber) > 0) System.out.print("Yes.");
        else System.out.print("No.");
    }
}
