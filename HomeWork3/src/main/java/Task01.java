public class Task01 {
    public static void main(String[] args) {

        int n = 157;

        System.out.println(Integer.toBinaryString(n));

        n = n >> 1;
        n = n << 1;

        System.out.println(Integer.toBinaryString(n));
    }
}
