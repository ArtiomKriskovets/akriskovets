public class Task10 {
    public static void main(String[] args) {
        int a = 12;
        int b = 26;
        int min = a & ((a-b) >> 31) | b & (~(a-b) >> 31);

        System.out.println(min);
        System.out.println(Integer.toBinaryString(min));
    }
}
