import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyTest extends TestCase {

    @Test
    void moneySum() {
        Money m1 = new Money(20, 30);
        Money m2 = new Money(15,5);
        Money result1 = new Money(35, 35);

        Money m3 = new Money(27, 93);
        Money m4 = new Money(3, 17);
        Money result2 = new Money(31,10);

        assertEquals(result1.toString(), Money.sum(m1, m2).toString());
        assertEquals(result2.toString(), Money.sum(m3, m4).toString());
    }

    @Test
    void moneySubstraction() {
        Money m1 = new Money(20, 5);
        Money m2 = new Money(15,30);
        Money result1 = new Money(4, 75);

        Money m3 = new Money(27, 93);
        Money m4 = new Money(3, 17);
        Money result2 = new Money(24,76);

        assertEquals(result1.toString(), Money.moneySubstraction(m1, m2).toString());
        assertEquals(result2.toString(), Money.moneySubstraction(m3, m4).toString());
    }

    @Test
    void isBiggerThan() {
        Money m1 = new Money(15,30);
        Money m2 = new Money(20, 5);
        Money testingMoney = new Money(17, 75);

        assertEquals(true, testingMoney.isBiggerThan(m1));
        assertEquals(false, testingMoney.isBiggerThan(m2));
    }

    @Test
    void isEqually() {
        Money m1 = new Money(17,75);
        Money m2 = new Money(20, 5);
        Money testingMoney = new Money(17, 75);

        assertEquals(true, testingMoney.isEqually(m1));
        assertEquals(false, testingMoney.isEqually(m2));
    }

}