import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexNumberTest extends TestCase {

    @Test
    void sum() {
        ComplexNumber number1 = new ComplexNumber(2, 3);
        ComplexNumber number2 = new ComplexNumber(5, 7);
        ComplexNumber result = new ComplexNumber(7, 10);

        assertEquals(true, result.isEqually(ComplexNumber.sum(number1, number2)));
    }

    @Test
    void subtraction() {
        ComplexNumber number1 = new ComplexNumber(7, 12);
        ComplexNumber number2 = new ComplexNumber(5, 7);
        ComplexNumber result = new ComplexNumber(2, 5);

        assertEquals(true, result.isEqually(ComplexNumber.subtraction(number1, number2)));
    }

    @Test
    void multiplication() {
        ComplexNumber number1 = new ComplexNumber(7, 12);
        ComplexNumber number2 = new ComplexNumber(5, 7);
        ComplexNumber result = new ComplexNumber(-49, 109);

        assertEquals(true, result.isEqually(ComplexNumber.multiplication(number1, number2)));

    }
}