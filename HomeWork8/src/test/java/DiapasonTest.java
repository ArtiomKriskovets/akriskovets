import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiapasonTest extends TestCase {

    @Test
    void isCorrectDiapasonPoints() {
        Diapason d1 = new Diapason(2, 10);
        Diapason d2 = new Diapason(10, 2);
        Diapason d3 = new Diapason(5, 5);

        assertEquals(true, Diapason.isCorrectDiapasonPoints(d1));
        assertEquals(false, Diapason.isCorrectDiapasonPoints(d2));
        assertEquals(false, Diapason.isCorrectDiapasonPoints(d3));
    }

    @Test
    void returnDiapasonLength() {
        Diapason d1 = new Diapason(2,10);
        Diapason d2 = new Diapason(5,15);
        Diapason d3 = new Diapason(-20,15);

        assertEquals(8, Diapason.returnDiapasonLength(d1));
        assertEquals(10, Diapason.returnDiapasonLength(d2));
        assertEquals(35, Diapason.returnDiapasonLength(d3));
    }

    @Test
    void isThereIntersection() {
        Diapason testingDiapason = new Diapason(10,17);
        Diapason d1 = new Diapason(4,8);
        Diapason d2 = new Diapason(6,11);
        Diapason d3 = new Diapason(11,16);
        Diapason d4 = new Diapason(14,18);
        Diapason d5 = new Diapason(18,22);

        assertEquals(false, Diapason.isThereIntersection(testingDiapason, d1));
        assertEquals(true, Diapason.isThereIntersection(testingDiapason, d2));
        assertEquals(true, Diapason.isThereIntersection(testingDiapason, d3));
        assertEquals(true, Diapason.isThereIntersection(testingDiapason, d4));
        assertEquals(false, Diapason.isThereIntersection(testingDiapason, d5));
    }
}