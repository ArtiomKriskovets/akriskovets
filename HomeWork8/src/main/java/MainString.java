import java.util.Arrays;

public class MainString {
    private char[] container;
    private int length;

    public MainString() {
        container = new char[0];
        length = 0;
    }

    public MainString(String string) {
        container = string.toCharArray();
        length = container.length;
    }

    public MainString(char character) {
        container = new char[1];
        container[0] = character;
        length = 1;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("");
        for (int i = 0; i < length; i++) {
            result.append(container[i]);
        }
        return result.toString();
    }

    public int getLength() {
        return length;
    }

    public void clear() {
        container = new char[0];
        length = 0;
    }

    public int findCharacterNumber(char character) {
        int result = -1;

        for (int i = 0; i < length; i++) {
            if (container[i] == character) {
                result = i;
                break;
            }
        }

        return result;
    }


}
