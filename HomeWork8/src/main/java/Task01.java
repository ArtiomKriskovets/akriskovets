import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        System.out.println("Input number of diapasons:");

        Scanner sc = new Scanner(System.in);

        Diapason[] diapasonArray = new Diapason[sc.nextInt()];

        for (int i = 0; i < diapasonArray.length; i++) {
            System.out.printf("Input %d diapason:%n", i + 1);
            diapasonArray[i] = new Diapason(sc.nextInt(), sc.nextInt());
        }

        for (int i = 0; i < diapasonArray.length; i++) {
            System.out.print(Diapason.returnDiapasonLength(diapasonArray[i]) + " ");
        }
    }
}
