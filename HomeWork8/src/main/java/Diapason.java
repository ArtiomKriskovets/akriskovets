public class Diapason {
    private int leftPoint;
    private int rightPoint;

    public Diapason(int leftPoint, int rightPoint) {
        this.leftPoint = leftPoint;
        this.rightPoint = rightPoint;
    }

    public void setDiapason(int x1, int x2) {
        this.leftPoint = leftPoint;
        this.rightPoint = rightPoint;
    }

    public int getLeftPoint() {
        return this.leftPoint;
    }

    public int getRightPoint() {
        return this.rightPoint;
    }

    public void setLeftPoint(int leftPoint) {
        this.leftPoint = leftPoint;
    }

    public void setRightPoint(int rightPoint) {
        this.rightPoint = rightPoint;
    }

    public static boolean isCorrectDiapasonPoints(Diapason diapason) {
        if (diapason.getLeftPoint() < diapason.getRightPoint()) {
            return true;
        } else {
            return false;
        }
    }

    public static int returnDiapasonLength (Diapason diapason) {
        return Math.abs(diapason.getRightPoint() - diapason.getLeftPoint());
    }

    public static boolean isThereIntersection (Diapason dOne, Diapason dTwo) {
        if (dOne.getLeftPoint() < dTwo.getRightPoint()) {
            if (dOne.getRightPoint() > dTwo.getLeftPoint()) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }




}
