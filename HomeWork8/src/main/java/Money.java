public class Money {
    private long grivna;
    private byte penny;

    public Money(long grivna, byte penny) {
        this.grivna = grivna;
        this.penny = (byte) (penny % 100);
        this.grivna += penny / 100;
    }

    public Money(byte grivna, byte penny) {
        this.grivna = grivna;
        this.penny = (byte) (penny % 100);
        this.grivna += penny / 100;
    }

    public Money(int grivna, int penny) {
        this.grivna = grivna;
        this.penny = (byte) (penny % 100);
        this.grivna += penny / 100;
    }

    public void print() {
        System.out.print(this);
    }

    @Override
    public String toString() {
        return grivna + "," + penny;
    }

    public long getGrivna() {
        return grivna;
    }

    public void setGrivna(long grivna) {
        this.grivna = grivna;
    }

    public byte getPenny() {
        return penny;
    }

    public void setPenny(byte penny) {
        this.penny = penny;
    }

    public static Money sum(Money moneyOne, Money moneyTwo) {
        long grivna = moneyOne.getGrivna() + moneyTwo.getGrivna();
        byte penny = (byte) (moneyOne.getPenny() + moneyTwo.getPenny());
        Money result = new Money(grivna, penny);
        return result;
    }

    public static Money pennyDecomposition(long penny) {
        long resultGrivna = penny / 100;
        byte resultPenny = (byte) (penny % 100);
        Money result = new Money(resultGrivna, resultPenny);
        return result;
    }

    public long moneyToPenny() {
        long result = this.getGrivna() * 100 + this.getPenny();
        return result;
    }

    public static Money moneySubstraction(Money moneyOne, Money moneyTwo) {
        long grivnaOne = moneyOne.getGrivna();
        long grivnaTwo = moneyTwo.getGrivna();
        int pennyOne = moneyOne.getPenny();
        byte pennyTwo = moneyTwo.getPenny();

        if (pennyOne < pennyTwo) {
            pennyOne += 100;
            grivnaOne--;
        }

        grivnaOne -= grivnaTwo;
        pennyOne -= pennyTwo;

        Money result = new Money(grivnaOne, (byte) pennyOne);
        return result;

    }

    public boolean isBiggerThan(Money money) {
        if (this.getGrivna() > money.getGrivna()) {
            return true;
        } else if ((this.getGrivna() == money.getGrivna()) && (this.getPenny() > money.getPenny())) {
            return  true;
        } else {
            return false;
        }
    }

    public boolean isEqually(Money money) {
        if ((this.getGrivna() == money.getGrivna()) && (this.getPenny() == money.getPenny())) {
            return true;
        } else {
            return false;
        }
    }

    public static Money division(Money money, float number) {
        long penny = money.moneyToPenny();
        penny /= number;
        Money result = Money.pennyDecomposition(penny);
        return result;
    }

    public static Money division(Money money, int number) {
        long penny = money.moneyToPenny();
        penny /= number;
        Money result = Money.pennyDecomposition(penny);
        return result;
    }

    public static Money multiplication(Money money, float number) {
        long penny = money.moneyToPenny();
        penny *= number;
        Money result = Money.pennyDecomposition(penny);
        return result;
    }

    public static Money multiplication(Money money, int number) {
        long penny = money.moneyToPenny();
        penny *= number;
        Money result = Money.pennyDecomposition(penny);
        return result;
    }

}
