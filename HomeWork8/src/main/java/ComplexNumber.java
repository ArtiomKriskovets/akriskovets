public class ComplexNumber {

    private double realPart;
    private double imaginaryPart;

    public ComplexNumber(double realPart, double imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public ComplexNumber(int realPart, int imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public ComplexNumber(int realPart, double imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public ComplexNumber(double realPart, int imaginaryPart) {
        this.realPart = realPart;
        this.imaginaryPart = imaginaryPart;
    }

    public double getImaginaryPart() {
        return imaginaryPart;
    }

    public void setImaginaryPart(double imaginaryPart) {
        this.imaginaryPart = imaginaryPart;
    }

    public double getRealPart() {
        return realPart;
    }

    public void setRealPart(double realPart) {
        this.realPart = realPart;
    }

    public static ComplexNumber sum(ComplexNumber numberOne, ComplexNumber numberTwo) {
        double resultRealPart = numberOne.getRealPart() + numberTwo.getRealPart();
        double resultImaginaryPart = numberOne.getImaginaryPart() + numberTwo.getImaginaryPart();
        ComplexNumber result = new ComplexNumber(resultRealPart, resultImaginaryPart);
        return result;
    }

    public static ComplexNumber subtraction(ComplexNumber numberOne, ComplexNumber numberTwo) {
        double resultRealPart = numberOne.getRealPart() - numberTwo.getRealPart();
        double resultImaginaryPart = numberOne.getImaginaryPart() - numberTwo.getImaginaryPart();
        ComplexNumber result = new ComplexNumber(resultRealPart, resultImaginaryPart);
        return result;
    }

    public static ComplexNumber multiplication(ComplexNumber numberOne, ComplexNumber numberTwo) {
        double x1 = numberOne.getRealPart();
        double x2 = numberTwo.getRealPart();
        double y1 = numberOne.getImaginaryPart();
        double y2 = numberTwo.getImaginaryPart();
        double xResult = x1 * x2 - y1 * y2;
        double yResult = x1 * y2 + x2 * y1;
        ComplexNumber result = new ComplexNumber(xResult, yResult);
        return result;
    }

    public boolean isEqually(ComplexNumber number) {
        if ((this.getRealPart() == number.getRealPart()) && (this.getImaginaryPart() == number.getImaginaryPart())) {
            return true;
        } else {
            return false;
        }
    }

}
