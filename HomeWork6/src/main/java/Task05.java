import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int side = sc.nextInt();

        for (int i = 1; i <= side; i++) {
            for (int j = 1; j <= side*4; j++) {
                if ((i == (side - j+1))|
                        (i == (side * 4 - j+1))|
                        ((i == side)&(j <= side * 3))|
                        ((i == 1)&(j > side)))
                    System.out.print('*');
                else System.out.print(" ");
            }
            System.out.println();
        }

        System.out.println();

        for (int i = 1; i <= side; i++) {
            for (int j = 1; j <= side * 4; j++){
                if ((i <= (side - j + 1)) | (i >= (side * 4 - j + 1))) System.out.print(" ");
                else System.out.print('*');
            }

            System.out.println();
        }


    }
}
