import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();

        int summ = 0;
        int count = 0;

        while (number != 0){
            summ += number % 10;
            count++;
            number /= 10;
        }

        System.out.printf("%d %d", summ, count);
    }
}
