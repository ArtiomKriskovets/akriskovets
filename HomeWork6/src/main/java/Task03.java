import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int side = sc.nextInt();

        for (int i = 1; i <= side; i++) {
            for (int j = 1; j <= side*2; j++) {
                if ((i == (side - j+1))|(i+side == j)|(i == side)) System.out.print('*');
                else System.out.print(" ");
            }
            System.out.println();
        }

        System.out.println();

        for (int i = 1; i <= side; i++) {
            for (int j = 1; j <= side*2; j++) {
                if (((i >= (side - j+1))&(i+side >= j))|(i == side)) System.out.print('*');
                else System.out.print(" ");
            }
            System.out.println();
        }
    }
}
