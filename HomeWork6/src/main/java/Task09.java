import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int partsNumber = sc.nextInt();

        int height = sc.nextInt();

        int displacement = partsNumber - 1;

        for (int part = 0; part < partsNumber; part++) {

            for (int i = 1; i <= (height + part) ; i++) {
                for (int j = 1; j <= (height + part + displacement) * 2 - 1; j++) {
                    if ((j >= displacement + ((height + part)- (i - 1))) & (j <= displacement + (height + part) + (i - 1))) System.out.print("*");
                    else System.out.print(" ");
                }

                System.out.println();
            }
            displacement--;

        }
    }
}
