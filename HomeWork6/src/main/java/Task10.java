import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        long mainNumber = sc.nextLong();

            for (int i = 1; i<=8; i++){
                long tempNumber = reverseNumber(mainNumber);
                while (tempNumber!=0) {
                    System.out.print(returnNumberString(i, (int)tempNumber%10));
                    tempNumber /=10;
                }
                System.out.println();
            }

    }

    private static String returnZeroString (int line){
        String result;
        switch(line){
            case 1:
                result = " **  ";
                break;
            case 2:
                result = "*  * ";
                break;
            case 3:
                result = "*  * ";
                break;
            case 4:
                result = "*  * ";
                break;
            case 5:
                result = "*  * ";
                break;
            case 6:
                result = " **  ";
                break;
                default: result =  "";
                break;

        }
        return result;
    }

    private static String returnOneString (int line){
        String result;
        switch(line){
            case 1:
                result = " *  ";
                break;
            case 2:
                result = "**  ";
                break;
            case 3:
                result = " *  ";
                break;
            case 4:
                result = " *  ";
                break;
            case 5:
                result = " *  ";
                break;
            case 6:
                result = "*** ";
                break;
            default: result =  "";
                break;

        }
        return result;
    }

    private static String returnTwoString (int line){
        String result;
        switch(line){
            case 1:
                result = " **  ";
                break;
            case 2:
                result = "*  * ";
                break;
            case 3:
                result = "   * ";
                break;
            case 4:
                result = "  *  ";
                break;
            case 5:
                result = " *   ";
                break;
            case 6:
                result = "**** ";
                break;
            default: result =  "";
                break;
        }
        return result;
    }

    private static String returnTreeString (int line){
        String result;
        switch(line){
            case 1:
                result = " **  ";
                break;
            case 2:
                result = "*  * ";
                break;
            case 3:
                result = "  *  ";
                break;
            case 4:
                result = "   * ";
                break;
            case 5:
                result = "*  * ";
                break;
            case 6:
                result = " **  ";
                break;
            default: result =  "";
                break;
        }
        return result;
    }

    private static String returnFourString (int line){
        String result;
        switch(line){
            case 1:
                result = "  ** ";
                break;
            case 2:
                result = " * * ";
                break;
            case 3:
                result = "*  * ";
                break;
            case 4:
                result = "**** ";
                break;
            case 5:
                result = "   * ";
                break;
            case 6:
                result = "   * ";
                break;
            default: result =  "";
                break;
        }
        return result;
    }

    private static String returnFiveString (int line){
        String result;
        switch(line){
            case 1:
                result = "**** ";
                break;
            case 2:
                result = "*    ";
                break;
            case 3:
                result = "***  ";
                break;
            case 4:
                result = "   * ";
                break;
            case 5:
                result = "*  * ";
                break;
            case 6:
                result = " **  ";
                break;
            default: result =  "";
                break;
        }
        return result;
    }

    private static String returnSixString (int line){
        String result;
        switch(line){
            case 1:
                result = " **  ";
                break;
            case 2:
                result = "*    ";
                break;
            case 3:
                result = "***  ";
                break;
            case 4:
                result = "*  * ";
                break;
            case 5:
                result = "*  * ";
                break;
            case 6:
                result = " **  ";
                break;
            default: result =  "";
                break;
        }
        return result;
    }

    private static String returnSevenString (int line){
        String result;
        switch(line){
            case 1:
                result = "**** ";
                break;
            case 2:
                result = "   * ";
                break;
            case 3:
                result = "  *  ";
                break;
            case 4:
                result = " *   ";
                break;
            case 5:
                result = " *   ";
                break;
            case 6:
                result = " *   ";
                break;
            default: result =  "";
                break;
        }
        return result;
    }

    private static String returnEightString (int line){
        String result;
        switch(line){
            case 1:
                result = " **  ";
                break;
            case 2:
                result = "*  * ";
                break;
            case 3:
                result = " **  ";
                break;
            case 4:
                result = "*  * ";
                break;
            case 5:
                result = "*  * ";
                break;
            case 6:
                result = " **  ";
                break;
            default: result =  "";
                break;

        }
        return result;
    }

    private static String returnNineString (int line){
        String result;
        switch(line){
            case 1:
                result = " **  ";
                break;
            case 2:
                result = "*  * ";
                break;
            case 3:
                result = "*  * ";
                break;
            case 4:
                result = " *** ";
                break;
            case 5:
                result = "   * ";
                break;
            case 6:
                result = " **  ";
                break;
            default: result =  "";
                break;

        }
        return result;
    }

    private static String returnNumberString (int line, int number){
        String result;
        switch (number){
            case 0:
                result = returnZeroString(line);
                break;
            case 1:
                result = returnOneString(line);
                break;
            case 2:
                result = returnTwoString(line);
                break;
            case 3:
                result = returnTreeString(line);
                break;
            case 4:
                result = returnFourString(line);
                break;
            case 5:
                result = returnFiveString(line);
                break;
            case 6:
                result = returnSixString(line);
                break;
            case 7:
                result = returnSevenString(line);
                break;
            case 8:
                result = returnEightString(line);
                break;
            case 9:
                result = returnNineString(line);
                break;
                default: result = "";
                break;
        }
        return result;
    }

    private static long reverseNumber (long number){


        int statement = findStatement(number);


        int result = 0;

        while (number!=0){
            statement /= 10;
            result += (number%10) * statement;
            number/=10;
        }

        return result;
    }

    private static int findStatement (long number){
        int result = 1;
        while (number!=0){
            number /= 10;
            result *=10;
        }
        return result;
    }
}
