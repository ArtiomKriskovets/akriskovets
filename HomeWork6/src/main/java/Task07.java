import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();

        int tempNumber = number;

        int statement = 1;
        while (tempNumber!=0){
            tempNumber /= 10;
            statement *=10;
        }

        int result = 0;
        while (number!=0){
            statement /= 10;
            result += (number%10) * statement;
            number/=10;
        }

        System.out.print(result);
    }
}
