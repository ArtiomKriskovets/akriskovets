import java.util.Scanner;

public class Task08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String mainString = sc.next();

        boolean flag = mainString.contains("_");

        char[] identifierArray = mainString.toCharArray();

        if (Character.isUpperCase(identifierArray[0])) {
            System.out.print("Incorrect input");
            System.exit(0);
        }

        if (isMixedCase(identifierArray)){
            System.out.print("It is mixed case");
            System.exit(0);
        }

        if (flag) System.out.print(returnCamelCase(identifierArray));
        else System.out.print(returnSnakeCase(identifierArray));
    }

    private static boolean isMixedCase (char[] testingArray){
        boolean result = false;
        boolean flag1 = false;
        boolean flag2 = false;

        for (int i = 0; i < testingArray.length; i++) {
            if (Character.isUpperCase(testingArray[i])) flag1 = true;
            if (testingArray[i] == '_') flag2 = true;
        }
         if (flag1 & flag2) result = true;

        return result;
    }

    private static String returnCamelCase (char[] inputArray){
        StringBuilder result = new StringBuilder("");

        int index = 0;

        while (index < inputArray.length){
            if (inputArray[index] != '_') result.append(inputArray[index]);
            else {
                index++;
                result.append(Character.toUpperCase(inputArray[index]));
            }
            index++;
        }
        return result.toString();
    }

    private static String returnSnakeCase (char[] inputArray){
        StringBuilder result = new StringBuilder("");

        int index = 0;

        while (index < inputArray.length){
            if (Character.isLowerCase(inputArray[index])) result.append(inputArray[index]);
            else {
                result.append("_");
                result.append(Character.toLowerCase(inputArray[index]));
            }
            index++;
        }
        return result.toString();
    }
}
