import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        char[] mainCharArray = sc.nextLine().toCharArray();

        int lengthIndex = 0;
        int count = 0;
        String end = "()[]";

        while (lengthIndex < mainCharArray.length){
            if ((mainCharArray[lengthIndex] == ':')|(mainCharArray[lengthIndex] == ';'))
                if (lengthIndex + 1 < mainCharArray.length) {
                    lengthIndex++;
                    while (mainCharArray[lengthIndex] == '-'){
                        lengthIndex++;
                    }
                    if ((lengthIndex < mainCharArray.length)&&(end.contains(Character.toString(mainCharArray[lengthIndex])))) count++;
                } else break;
            lengthIndex++;
        }

        System.out.print(count);
    }
}
