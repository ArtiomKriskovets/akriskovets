import java.util.Scanner;

public class Task07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String mainString = sc.nextLine();

        char[] mainCharArray = mainString.toCharArray();

        for (int i = 0; i < mainCharArray.length; i++){
            if (Character.isDigit(mainCharArray[i])) System.out.print("_");
            else if (Character.isLetter(mainCharArray[i])) {
                if (Character.isLowerCase(mainCharArray[i])) System.out.print(Character.toUpperCase(mainCharArray[i]));
                else System.out.print(Character.toLowerCase(mainCharArray[i]));
            }
            else System.out.print(mainCharArray[i]);
        }
    }
}
