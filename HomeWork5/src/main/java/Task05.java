import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String mainString = sc.nextLine();

        String[] stringArray = mainString.split("[ \t:]");

        int flag = 0;

        for (int i = 0; i < stringArray.length; i++){
            if (stringArray[i].length()%2 == 0) flag++;
        }

        System.out.print(flag);

    }
}
