import java.util.Scanner;

public class Task01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String mainString = sc.nextLine();
        char testSymbol = sc.next().charAt(0);
        short flag = 0;

        char[] mainStringArr = mainString.toCharArray();

        for (int i = 0; i < mainString.length(); i++){
            if (testSymbol == mainStringArr[i]) {
                flag++;
                System.out.printf("%d ", i);

            }
        }

        if (flag != 0) System.out.printf("%nNumber of coincidences = %d", flag);
        else System.out.print("There is no coincidences");
    }
}
