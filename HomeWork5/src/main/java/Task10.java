import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        char[] mainCharArray = sc.next().toCharArray();

        for (int i = 0; i < mainCharArray.length; i++) {
            for (int j = 0; j < (mainCharArray.length - (1+i)); j++)
                System.out.print(" ");
            for (int j = 0; j < mainCharArray.length; j++)
                if (j <= i) System.out.print(mainCharArray[j]);

            System.out.println();
        }


        for (int i = 1; i < mainCharArray.length; i++) {
            for (int j = 0; j < mainCharArray.length; j++) {
                if (j >= i) System.out.print(mainCharArray[j]);
            }
            System.out.println();
        }
    }
}
