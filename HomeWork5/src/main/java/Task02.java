import java.util.Scanner;

public class Task02 {
    public static void main(String[] args) {
        StringBuilder mainString = new StringBuilder("");

        int i = 1;

        while (mainString.length() <= 1000){
            mainString.append(i);
            i++;
        }

        Scanner sc = new Scanner(System.in);

        int n = sc.nextInt();

        System.out.print(mainString.charAt(n - 1));
    }
}
