import java.util.Scanner;

public class Task04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String mainString = sc.nextLine();

        String[] stringArray = mainString.replaceAll("[-.?!)(,:]", "").split("[ \t]");

        String firstLetterTest = "aeiou";
        String lastLetterTest = "bcdfghjklmnpqrstvwxyz";

        for (int i = 0; i < stringArray.length; i++){
            String firstLetter = Character.toString(stringArray[i].charAt(0));
            String lastLetter = Character.toString(stringArray[i].charAt(stringArray.length - 1));
            if ((firstLetterTest.contains(firstLetter))&&(lastLetterTest.contains(lastLetter))){
                System.out.println(stringArray[i]);
            }
        }
    }
}
