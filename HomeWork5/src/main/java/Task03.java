import java.util.Scanner;

public class Task03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String mainString = sc.nextLine();

        String[] stringArray = mainString.replaceAll("[-.?!)(,:]", "").split("[ \t]");

        short flag = 0;
        int summOfLength = 0;

        for (int i = 0; i < stringArray.length; i++){
            flag++;
            summOfLength +=stringArray[i].length();
        }

        if (flag != 0) System.out.print(summOfLength/(float)flag);
        else System.out.print("There is no words");
    }
}
