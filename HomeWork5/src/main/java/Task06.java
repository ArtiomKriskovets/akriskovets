public class Task06 {
    public static void main(String[] args) {
        StringBuilder[] mainStringArray = fillStringArray();

        int flag = 0;

        for (int i = 0; i < mainStringArray.length; i++){
            if (checkOut(mainStringArray[i].toString())) flag++;
        }

        System.out.print(flag);
    }

    private static StringBuilder[] fillStringArray(){
        StringBuilder[] resultString = new StringBuilder[99999];

        for (int i = 0; i < 99999; i++){
            resultString[i] = new StringBuilder("");
            if (i < 10) resultString[i].append("0000");
            if ((i >= 10)&&( i < 100)) resultString[i].append("000");
            if ((i >= 100)&&(i < 1000)) resultString[i].append("00");
            if ((i >= 1000)&&(i < 10000)) resultString[i].append("0");
            resultString[i].append(i);

        }

        return resultString;
    }

    private static boolean checkOut (String testString){
        boolean flag = false;

        char[] testArray = testString.toCharArray();

        for (int i = 0; i < testArray.length; i++){
            if (testArray[i] == '4'){
                return true;
            }
            if ((testArray[i] == 1)&&((i + 1) < testArray.length)&&(testArray[i + 1] == '3')){
                return true;
            }
        }

        return flag;
    }
}


