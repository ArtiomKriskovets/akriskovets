import java.util.Arrays;
import java.util.Scanner;

public class Task09 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        char[] testStringOne = allToLowerCase(sc.nextLine().replaceAll("[-.?!)(,: \t]", "").toCharArray());
        char[] testStringTwo = allToLowerCase(sc.nextLine().replaceAll("[-.?!)(,: \t]", "").toCharArray());

        Arrays.sort(testStringOne);
        Arrays.sort(testStringTwo);

        int flag = 0;

        for (int i = 0; i < testStringOne.length; i++){
            if (testStringOne[i] != testStringTwo[i]) flag++;
        }

        if (flag == 0) System.out.print("Yes");
        else System.out.print("No");

    }

    private static char[] allToLowerCase(char[] input){
        char[] result = new char[input.length];

        for (int i = 0; i < input.length; i++){
            result[i] = Character.toLowerCase(input[i]);
        }

        return result;
    }
}
