public class BattleField {
    private int dragonHealth;
    private int dragonAttack;
    private int spearmanHealth;
    private int spearmanAttack;
    private int spearmanNumber;
    private int healthOfLastSpearman;

    public BattleField(int dragonHealth, int gragonAttack, int spearmanHealth, int spearmanAttack) {
        this.dragonHealth = dragonHealth;
        this.dragonAttack = gragonAttack;
        this.spearmanHealth = spearmanHealth;
        this.spearmanAttack = spearmanAttack;
        healthOfLastSpearman = 0;
    }

    public BattleField() {

    }

    public void setDragonHealth(int dragonHealth) {
        this.dragonHealth = dragonHealth;
    }

    public void setDragonAttack(int gragonAttack) {
        this.dragonAttack = gragonAttack;
    }

    public void setSpearmanHealth(int spearmanHealth) {
        this.spearmanHealth = spearmanHealth;
    }

    public void setSpearmanAttack(int spearmanAttack) {
        this.spearmanAttack = spearmanAttack;
    }

    public void setSpearmanNumber(int spearmanNumber) {
        this.spearmanNumber = spearmanNumber;
    }

    public int getDragonHealth() {
        return dragonHealth;
    }

    public int getDragonAttack() {
        return dragonAttack;
    }

    public int getSpearmanHealth() {
        return spearmanHealth;
    }

    public int getSpearmanAttack() {
        return spearmanAttack;
    }

    public int getSpearmanNumber() {
        return spearmanNumber;
    }

    private void calculateDragonAttack() {
        int allSpearmanHealth = spearmanHealth * spearmanNumber;
        if (healthOfLastSpearman != 0) {
            allSpearmanHealth -= spearmanHealth;
            allSpearmanHealth += healthOfLastSpearman;
        }

        allSpearmanHealth -= dragonAttack;

        if (allSpearmanHealth == 0) {
            spearmanNumber = 0;
            healthOfLastSpearman = 0;
        } else {
            spearmanNumber = allSpearmanHealth / spearmanHealth;
            healthOfLastSpearman = allSpearmanHealth % spearmanHealth;
            if (healthOfLastSpearman != 0) {
                spearmanNumber++;
            }
        }

    }

    public void doDragonAttack() {
        calculateDragonAttack();
        if (spearmanNumber > 0) {
            System.out.printf("Dragon deals %d damage. %d spearman left", this.dragonAttack, this.spearmanNumber);
            if (spearmanNumber >= 0) {
                if (healthOfLastSpearman > 0) {
                    System.out.printf(", one of which is injured (%d lives left)%n", healthOfLastSpearman);
                } else {
                    System.out.printf(".%n");
                }
            } else {
                System.out.printf(".%n");
                System.out.print("Dragon wins.");
            }
        } else {
            System.out.print("Dragon attacks and win.");
        }

    }

    public void doSpearmanAttack() {
        calculateSpearmanAttack();
        if (dragonHealth > 0) {
            System.out.printf("Spearmans deal %d damage. %d gragon health left.%n", this.spearmanNumber * this.spearmanAttack, dragonHealth);
            if (dragonHealth == 0) {
                System.out.print("Spearmans win.");
            }
        } else {
            System.out.print("Srearmans attak and win.");
        }
    }

    private void calculateSpearmanAttack() {
        dragonHealth -= spearmanAttack * spearmanNumber;
    }
}
