import java.util.Scanner;

public class Task06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String mainString = sc.next();

        checkStringExpression(mainString);
    }

    private static void checkStringExpression(String inputString) {
        String checkString = "";

        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == '(') {
                checkString += '(';
            } else if (inputString.charAt(i) == '[') {
                checkString += '[';
            }

            if (inputString.charAt(i) == ')') {
                if (checkString.charAt(checkString.length() - 1) == '(') {
                    checkString = checkString.substring(0, checkString.length() - 1);
                } else {
                    System.out.print("Error. ] is missing.");
                    System.exit(0);
                }
            }

            if (inputString.charAt(i) == ']') {
                if (checkString.charAt(checkString.length() - 1) == '[') {
                    checkString = checkString.substring(0, checkString.length() - 1);
                } else {
                    System.out.print("Error. ) is missing.");
                    System.exit(0);
                }
            }
        }

        if (checkString.length() == 0) {
            System.out.print("There is no mistakes.");
        } else {
            System.out.print("Wrong string.");
        }
    }
}
