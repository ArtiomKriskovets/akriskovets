public class Task04 {
    public static void main(String[] args) {

        int[][] a = {{7}, {3, 8}, {8, 1, 0}, {2, 7, 4, 4}, {4, 5, 2, 6, 5}};

        int[] trace = new int[a.length];

        int maxTrace = getMaxTrace(a, 0, 0, trace);

        printTriangleArray(a);
        System.out.println(maxTrace);
        System.out.println(returnTraceString(trace));
    }

    private static int getMaxTrace(int [][] a, int col, int row, int[] inputTrace) {
        int res = a[row][col];
        int[] trace = new int[a.length];
        if (row != a.length - 1) {
            int left = getMaxTrace(a, col, row + 1, trace);
            int right = getMaxTrace(a, col + 1, row + 1, trace);
            res += Math.max(left, right);
            if (left > right) {
                trace[row] = 0;
            } else {
                trace[row] = 1;
            }
        }
        System.arraycopy(trace, 0,inputTrace, 0, inputTrace.length);
        return res;
    }

    private static String returnTraceString(int[] inputArray) {
        StringBuilder resultString = new StringBuilder("");

        for (int i = 0; i < inputArray.length - 1; i++) {
            if (inputArray[i] == 0) {
                resultString.append("left ");
            } else {
                resultString.append("right ");
            }
        }
        return resultString.toString();
    }

    private static void printTriangleArray(int[][] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            for (int k = 1; k < inputArray.length - i ; k++) {
                System.out.print(" ");
            }
            for (int j = 0; j < i + 1; j++) {
                System.out.printf("%d ", inputArray[i][j]);

            }
            System.out.println();
        }
        System.out.println();
    }
}
