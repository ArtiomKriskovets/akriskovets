import java.util.Random;

public class Task02 {
    public static void main(String[] args) {
        int[] integerArray = {2, 4, 6, 3, 6};
        String[] stringArray = {"One", "Two", "Three"};
        int[][] integerTwoDimensionalArray = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        float[][] floatTwoDimensionalArray = generateFloatArray(3, 5);

        printArray(integerArray);
        nextLine();
        printArray(stringArray);
        nextLine();
        printArray(integerTwoDimensionalArray);
        nextLine();
        printArray(floatTwoDimensionalArray);
    }

    private static void printArray(int[] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            System.out.printf("%d ", inputArray[i]);
        }
        System.out.println();
    }

    private static void printArray(String[] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            System.out.printf("%s%n", inputArray[i]);
        }
    }

    private static void printArray(int[][] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[i].length; j++) {
                System.out.printf("%d ", inputArray[i][j]);
            }
            System.out.println();
        }
    }

    private static void printArray(float[][] inputArray) {
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[i].length; j++) {
                System.out.printf("%f ", inputArray[i][j]);
            }
            System.out.println();
        }
    }

    private static void nextLine() {
        System.out.println();
    }

    private static float[][] generateFloatArray(int height, int width) {
        Random rnd = new Random();
        float[][] resultArray = new float[height][width];

        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                resultArray[i][j] = rnd.nextFloat();
            }
        }
        return resultArray;
    }
}
