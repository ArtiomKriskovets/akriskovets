import java.util.Scanner;

public class Task05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        BattleField battle = new BattleField();

        System.out.println("Input dragon health:");
        battle.setDragonHealth(sc.nextInt());
        System.out.println("Input dragon attack:");
        battle.setDragonAttack(sc.nextInt());
        System.out.println("Input health of a spearman:");
        battle.setSpearmanHealth(sc.nextInt());
        System.out.println("Input attack of a spearman:");
        battle.setSpearmanAttack(sc.nextInt());
        System.out.println("Input spearman number:");
        battle.setSpearmanNumber(sc.nextInt());

        while ((battle.getDragonHealth() > 0) && (battle.getSpearmanNumber() > 0)) {
            battle.doSpearmanAttack();
            if (battle.getDragonHealth() > 0) {
                battle.doDragonAttack();
            }
        }
    }






}
