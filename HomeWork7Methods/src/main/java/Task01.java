import java.util.Random;

public class Task01 {
    public static void main(String[] args) {

        System.out.println("Unit matrix");
        printMatrix(generateUnitMatrix(3));

        System.out.printf("%n");

        System.out.println("Zero matrix");
        printMatrix(generteZeroMatrix(3));

        System.out.printf("%n");

        int[][] matrixOne = generteRandomMatrix(3);
        int[][] matrixTwo = generteRandomMatrix(3);
        System.out.println("Matrix one:");
        printMatrix(matrixOne);
        System.out.println("Matrix two:");
        printMatrix(matrixTwo);
        System.out.println("Summ of matrixes:");
        printMatrix(returnSummOfMatrixes(matrixOne, matrixTwo));
        System.out.println("Multiplication of matrixes:");
        printMatrix(returnMatrixMultiplication(matrixOne, matrixTwo));
        System.out.println("Multiplication of scalar 2 and matrix one:");
        printMatrix(returnMatrixScalarMultiplication(matrixOne, 2));
        System.out.println("Determinant of matrix one");
        System.out.print(returnMatrixDeterm(matrixOne));


    }

    private static int[][] generteRandomMatrix (int size) {
        int[][] resultArray = new int[size][size];
        Random rnd = new Random();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                resultArray[i][j] = rnd.nextInt(10);
            }
        }

        return resultArray;
    }

    private static void printMatrix(int matrix[][]) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] < 10) {
                    System.out.print("  " + matrix[i][j] + " ");
                } else {
                    System.out.print(" " + matrix[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    private static int[][] generateUnitMatrix(int size) {
        int[][] resultArray = new int[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j) {
                    resultArray[i][j] = 1;
                } else {
                    resultArray[i][j] = 0;
                }
            }
        }

        return resultArray;
    }

    private static int[][] generteZeroMatrix (int size) {
        int[][] resultArray = new int[size][size];
        Random rnd = new Random();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                resultArray[i][j] = 0;
            }
        }
        return resultArray;
    }

    private static int[][] returnSummOfMatrixes(int[][] matrixOne, int[][] matrixTwo) {
        int[][] resultMatrix = generteZeroMatrix(matrixOne.length);

        for (int i = 0; i < matrixOne.length; i++) {
            for (int j = 0; j < matrixOne.length; j++) {
                resultMatrix[i][j] += matrixOne[i][j] + matrixTwo[i][j];
            }
        }
        return matrixOne;
    }

    private static int[][] returnMatrixMultiplication(int[][] matrixOne, int[][] matrixTwo) {
        int[][] matrixMultiplication = generteZeroMatrix(matrixOne.length);

        for (int matrixSize = 0; matrixSize < matrixOne.length; matrixSize++) {
            for (int i = 0; i < matrixOne.length; i++) {
                for (int j = 0; j < matrixOne.length; j++) {
                    matrixMultiplication[matrixSize][i] += matrixOne[matrixSize][j] * matrixTwo[j][i];
                }
            }
        }
        return matrixMultiplication;
    }

    private static int[][] returnMatrixScalarMultiplication(int[][] inputMatrix, int scalar) {
        int[][] resultMatrix = generteZeroMatrix(inputMatrix.length);

        for (int i = 0; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix.length; j++) {
                resultMatrix[i][j] += inputMatrix[i][j] * scalar;
            }
        }

        return resultMatrix;
    }

    private static int[][] returnMinor (int[][] inputMatrix, int line) {
        int[][] resultMatrix = new int[inputMatrix.length - 1][inputMatrix.length - 1];

        int iResult = 0;
        int jResult = 0;
        for (int i = 1; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix.length; j++) {
                if (j != line) {
                    resultMatrix[iResult][jResult] = inputMatrix[i][j];
                    jResult++;
                }
            }
            jResult = 0;
            iResult++;
        }
        return resultMatrix;
    }

    private static int returnMatrixDeterm (int[][] inputMatrix) {
        int result = 0;

        if (inputMatrix.length == 2) {
            result = inputMatrix[0][0] * inputMatrix[1][1] - inputMatrix[1][0] * inputMatrix[0][1];
        } else {
            int sign = 1;
            for (int i = 0; i < inputMatrix.length; i++) {
                result += sign * returnMatrixDeterm(returnMinor(inputMatrix, i)) * inputMatrix[0][i];
                sign *= -1;
            }
        }
        return result;
    }

 }
