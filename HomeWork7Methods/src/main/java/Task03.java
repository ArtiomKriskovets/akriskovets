import java.util.Random;

public class Task03 {
    public static void main(String[] args) {
        String[] mainStringArray = generateRandomStringArray(7);
        printStringArray(mainStringArray);
        sortByWords(mainStringArray);
        printStringArray(mainStringArray);

    }

    private static String[] generateRandomStringArray(int numberOfElements) {
        String[] resultStringArray = new String[numberOfElements];
        Random rnd = new Random();

        for (int i = 0; i < numberOfElements; i++) {
            resultStringArray[i] = generateRandomString(rnd.nextInt(5) + 1);
        }
        return resultStringArray;
    }

    private static String generateRandomWord(int length) {
        char[] resultWord = new char[length];

        String characters = "abcdefghijklmnopqrstuvwxyz";

        Random rnd = new Random();

        for (int i = 0; i < length; i++) {
            resultWord[i] = characters.charAt(rnd.nextInt(characters.length()));
        }

        return String.copyValueOf(resultWord);
    }

    private static String generateRandomString(int numberOfWords) {
        StringBuilder resultString = new StringBuilder("");
        Random rnd = new Random();

        for (int i = 0; i < numberOfWords; i++) {
            resultString.append(generateRandomWord(rnd.nextInt(6) + 1));
            if (i != (numberOfWords - 1)) {
                resultString.append(" ");
            }
        }

        return resultString.toString();
    }

    private static void printStringArray(String[] inputString) {
        for (int i = 0; i < inputString.length; i++) {
            System.out.printf("%s%n", inputString[i]);
        }
        System.out.println();
    }

    private static void sortByWords(String[] inputString) {
        String[][] stringArray = new String[inputString.length][];

        for (int i = 0; i < inputString.length; i++) {
            stringArray[i] = inputString[i].split(" ");
        }

        sortByLength(stringArray);

        for (int i = 0; i < inputString.length; i++) {
            inputString[i] = returnStringFromArray(stringArray[i]);
        }

    }

    private static void sortByLength(String[][] stringArray) {
        for (int i = stringArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (stringArray[j].length > stringArray[j + 1].length) {
                    String[] tempArray = stringArray[j];
                    stringArray[j] = stringArray[j + 1];
                    stringArray[j + 1] = tempArray;
                }
            }
        }
    }

    private static String returnStringFromArray (String[] inputArray) {
        StringBuilder resultString = new StringBuilder("");

        for (int i = 0; i < inputArray.length; i++) {
            resultString.append(inputArray[i]);
            if (i != inputArray.length - 1) {
                resultString.append(" ");
            }
        }
        return resultString.toString();
    }
}
